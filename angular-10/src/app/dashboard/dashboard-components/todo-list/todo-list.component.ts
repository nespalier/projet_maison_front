import { Component, OnInit } from '@angular/core';
import { RecentComment, recentComments } from './todo-list';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class ToDoList implements OnInit {

  recentComm: RecentComment[];

  constructor() {
    this.recentComm = recentComments;
  }

  ngOnInit(): void {
  }

}
