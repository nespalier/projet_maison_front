import { Component, OnInit } from '@angular/core';
import { ChataList, chatList } from './prestataires';

@Component({
  selector: 'app-prestataires',
  templateUrl: './prestataires.component.html',
  styleUrls: ['./prestataires.component.css']
})
export class Prestataires implements OnInit {

  chats: ChataList[];

  constructor() {
    this.chats = chatList;
  }

  ngOnInit(): void {
  }

}
