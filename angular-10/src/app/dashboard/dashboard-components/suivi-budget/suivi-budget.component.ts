import { Component, OnInit } from '@angular/core';
import { RecentSale, recentSales } from './suivi-budget';

@Component({
  selector: 'app-suivi-budget',
  templateUrl: './suivi-budget.component.html',
  styleUrls: ['./suivi-budget.component.css']
})
export class SuiviBudget implements OnInit {

  tableData: RecentSale[];

  constructor() {
    this.tableData = recentSales;
    console.log(this.tableData[4].Date.toDateString());
  }

  ngOnInit(): void {
  }

}
