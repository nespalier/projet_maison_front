import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { DashboardComponent } from './dashboard.component';
import { Progressions } from './dashboard-components/progressions/progressions.component';
import { SuiviBudget } from './dashboard-components/suivi-budget/suivi-budget.component';
import { ToDoList } from './dashboard-components/todo-list/todo-list.component';

import { Prestataires } from './dashboard-components/prestataires/prestataires.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Dashboard',
      urls: [
        { title: 'Dashboard', url: '/dashboard' },
        { title: 'Dashboard' }
      ]
    },
    component: DashboardComponent
  }
];

@NgModule({
  imports: [FormsModule, CommonModule, RouterModule.forChild(routes), ChartsModule],
  declarations: [DashboardComponent, Progressions,
    SuiviBudget, ToDoList, Prestataires]
})
export class DashboardModule {

}
