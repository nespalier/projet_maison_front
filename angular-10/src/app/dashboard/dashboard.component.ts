import { BienVO } from './../shared/bien/VO/bienVO';
import { BienService } from './../shared/bien/services/bien.service';
import { Component, AfterViewInit, OnInit } from '@angular/core';
//declare var require: any;

@Component({
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements AfterViewInit, OnInit {
  subtitle: string;
  biens:BienVO[]=[];
  constructor(
    private readonly bienService: BienService
  ) {
    this.subtitle = 'This is some text within a card block.';
  }
  ngOnInit(): void {
    this.listeBien();

  }

  ngAfterViewInit() {
  }

  listeBien(){
    this.bienService.listeBien().subscribe(
      result =>{
        this.biens = result;
        console.log(this.biens);
      }
    );
  }
}
