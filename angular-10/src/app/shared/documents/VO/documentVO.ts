import { StatutVO } from "./../../statut/VO/statutVO";
import { TypeDocumentVO } from "./../../type-document/VO/type-documentVO";
import { TypePrestataireVO } from "../../type-prestataire/VO/type-prestataireVO";
export class DocumentVO {
  id!: number;

  type!: TypeDocumentVO;

  statut!: StatutVO;

  documentPath!: string;

  contact!: string;

  description!: string;
}
