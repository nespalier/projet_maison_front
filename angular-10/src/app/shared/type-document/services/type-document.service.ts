import { TypeDocumentVO } from "./../VO/type-documentVO";
import { environment } from "../../../../environments/environment";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class TypeDocumentService{
  constructor(private http: HttpClient, private route: Router) {}

  pathTypeDocument: string = environment.path + "/typeDocument";

  listeTypeDocument() {
    return this.http.get<TypeDocumentVO[]>(this.pathTypeDocument);
  }

  recupereTypeDocumentParId(id: number) {
    return this.http.get<TypeDocumentVO>(this.pathTypeDocument + "/" + id);
  }

  ajouterModifierTypeDocument(typeDocument: TypeDocumentVO) {
    return this.http.post<TypeDocumentVO>(this.pathTypeDocument, typeDocument);
  }

  supprimerTypeDocumentParId(id:number){
    return this.http.delete(this.pathTypeDocument + "/" + id)
  }
}
