import { TypePrestataireVO } from './../VO/type-prestataireVO';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class TypePrestataireService {

  constructor(private http: HttpClient, private route: Router) { }

  pathTypePrestataire:string = "/typePrestaire"

  listeTypePrestataire(){
    return this.http.get<TypePrestataireVO[]>(environment.path+this.pathTypePrestataire);
  }

}
