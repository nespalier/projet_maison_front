import { DocumentVO } from "./../../documents/VO/documentVO";
import { BienVO } from "./../../bien/VO/bienVO";
import { PrestataireVO } from "./../../prestataires/VO/prestataireVO";
export class BienPredtataireVO {
  id!: number;

  prestataire!: PrestataireVO;

  bien!: BienVO;

  document!: DocumentVO;

  date!: Date;

  evenement: string;

  montant: number;
}
