import { environment } from '../../../../environments/environment';
import { BienVO } from '../VO/bienVO';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class BienService {

  constructor(private http: HttpClient, private route: Router) { }

  pathBien:string = "/bien"

  listeBien(){
    return this.http.get<BienVO[]>(environment.path+this.pathBien);
  }

}
