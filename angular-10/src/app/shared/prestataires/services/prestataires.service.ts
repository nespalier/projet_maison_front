import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../../../environments/environment";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class PtrestatairesService {

  constructor(private http: HttpClient, private route: Router) {
  }
  pathPrestataire: string = environment.path + "/prestataire";

}
