import { TypePrestataireVO } from "./../../type-prestataire/VO/type-prestataireVO";
export class PrestataireVO {
  id!: number;

  type!: TypePrestataireVO;

  numTel!: number;

  mail!: string;

  contact!: string;

  description!: string;
}
