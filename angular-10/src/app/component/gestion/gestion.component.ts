import { TypePrestataireService } from "./../../shared/type-prestataire/services/type-prestataire.service";
import { TypePrestataireVO } from "./../../shared/type-prestataire/VO/type-prestataireVO";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { TypeDocumentVO } from "./../../shared/type-document/VO/type-documentVO";
import { TypeDocumentService } from "./../../shared/type-document/services/type-document.service";
import { Component, OnInit } from "@angular/core";
@Component({
  templateUrl: "gestion.component.html",
})
export class GestionComponent implements OnInit {
  typeDocument: TypeDocumentVO = new TypeDocumentVO();

  typePrestataire: TypePrestataireVO = new TypePrestataireVO();
  listeTypeDocument!: TypeDocumentVO[];
  closeResult: string = "";
  formAjoutModifTypeDocument!: FormGroup;
  formAjoutModifTypePrestataire!: FormGroup;
  constructor(
    private readonly typeDocumentService: TypeDocumentService,
    private readonly typePrestataireService: TypePrestataireService,
    private modalService: NgbModal,
    private fc: FormBuilder
  ) {
    this.formAjoutModifTypeDocument = this.fc.group({
      id: [null],
      libelle: ["", Validators.required],
    });

    this.formAjoutModifTypePrestataire = this.fc.group({
      id: [null],
      libelle: ["", Validators.required],
    });
  }
  ngOnInit(): void {
    this.recupererlisteTypeDocument();
  }

  open1(content1: string) {
    this.modalService
      .open(content1, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  ajouterModifierTypeDoc() {
    this.typeDocument.id = this.formAjoutModifTypeDocument.value.id;
    this.typeDocument.libelle = this.formAjoutModifTypeDocument.value.libelle;

    this.typeDocumentService
      .ajouterModifierTypeDocument(this.typeDocument)
      .subscribe();
  }

  recupererlisteTypeDocument() {
    this.typeDocumentService.listeTypeDocument().subscribe((result) => {
      this.listeTypeDocument = result;
    });
  }

  private getDismissReason(reason: ModalDismissReasons): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
